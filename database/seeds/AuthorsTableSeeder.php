<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            [
                'id' => 1,
                'first_name' => 'J.K.',
                'last_name' => 'Rowling',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 2,
                'first_name' => 'Rick',
                'last_name' => 'Riordan',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 3,
                'first_name' => 'Elizabeth',
                'last_name' => 'Phillips',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
