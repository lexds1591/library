<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'name' => 'Kiss an Angel',
                'authors_id' => 3,
                'year' => 1996,
                'price' => 200.00,
                'editorials_id' => 1,
                'pages' => 352,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'The casual vacancy',
                'authors_id' => 1,
                'year' => 2012,
                'price' => 315.00,
                'editorials_id' => 2,
                'pages' => 458,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Nobody\'s baby but mine',
                'authors_id' => 3,
                'year' => 1997,
                'price' => 255.00,
                'editorials_id' => 1,
                'pages' => 354,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
