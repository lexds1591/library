<?php

use Illuminate\Database\Seeder;

class EditorialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('editorials')->insert([
            [
                'id' => 1,
                'name' => 'Alfaguara',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 2,
                'name' => 'Rocca Editorial',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'id' => 3,
                'name' => 'Planeta',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
