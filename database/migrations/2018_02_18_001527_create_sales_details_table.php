<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_id')->unsigned();
            $table->integer('books_id')->unsigned();
            $table->integer('quantity');
            $table->double('unit_price',5,2);

            //Foreign keys
            $table->foreign('sales_id')
                ->references('id')->on('sales')
                ->onDelete('cascade');
            $table->foreign('books_id')
                ->references('id')->on('books')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_details', function (Blueprint $table){
            $table->dropForeign(['sales_id'], ['books_id']);
        });
        Schema::dropIfExists('sales_details');
    }
}
