<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->integer('authors_id')->unsigned();
            $table->year('year');
            $table->double('price', 5, 2)->unsigned();
            $table->integer('editorials_id')->unsigned();
            $table->integer('pages')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            //Foreign keys
            $table->foreign('authors_id')
                ->references('id')->on('authors')
                ->onDelete('cascade');
            $table->foreign('editorials_id')
                ->references('id')->on('editorials')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table){
            $table->dropForeign(['authors_id'], ['editorials_id']);
        });
        Schema::dropIfExists('books');
    }
}
