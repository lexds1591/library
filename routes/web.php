<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

//Editorials routes
Route::get('/editorials', 'EditorialsController@index');
Route::get('/editorials/{id}', 'EditorialsController@find');
Route::post('/editorials', 'EditorialsController@store');
Route::put('/editorials/{id}', 'EditorialsController@update');
Route::delete('/editorials/{id}', 'EditorialsController@delete');

//Authors routes
Route::get('/authors','AuthorsController@index');
Route::get('/authors/{id}','AuthorsController@find');
Route::post('/authors','AuthorsController@store');
Route::put('/authors/{id}','AuthorsController@update');
Route::delete('/authors/{id}','AuthorsController@delete');

//Books routes
Route::get('/books','BooksController@index');
Route::get('/books/{id}','BooksController@find');
Route::post('/books','BooksController@store');
Route::put('/books/{id}','BooksController@update');

//Sales routes
Route::get('/sales','SalesController@index');
Route::post('/sales','SalesController@store');

//Reports routes
Route::get('/reports','ReportsController@index');

