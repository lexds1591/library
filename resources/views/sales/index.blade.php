<!-- Header -->
@include('header.index',['title' => $title])
<body>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <div class="container-fluid">
    <div class="row">
        <div class="col-md-1 menu-container" style="height: 100%;">
            @include('menu.index')
        </div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title text-center"><h4>Wellcome to {{$title}}.</h4></div>
                    </div>
                    @if(isset($message))
                    <div class="row">
                      <div class="col-md-12 head-title text-center"><h4>{{$message}}.</h4></div>
                    </div>
                    @endif
                    <script>
                    $(document).ready(function(){
                      $(".update").on("click",function(e){
                        $(".formValues").css("display","inline");
                        var id =  $(e)[0].currentTarget.dataset.id;
                        var name =  $(e)[0].currentTarget.dataset.name;
                        var author =  $(e)[0].currentTarget.dataset.author;
                        var year =  $(e)[0].currentTarget.dataset.year;
                        var price =  $(e)[0].currentTarget.dataset.price;
                        var editorial =  $(e)[0].currentTarget.dataset.editorial;
                        var pages =  $(e)[0].currentTarget.dataset.pages;

                        $('.formValues').attr('action', "/books/"+id);
                        $('#check input').attr('value','put')
                        $("#id").val(id);
                        $("#name").val(name);
                        $("#author_id").val(author);
                        $("#year").val(year);
                        $("#price").val(price);
                        $("#editorial_id").val(editorial);
                        $("#pages").val(pages);
                        $("#create").val("Update");
                        $("#cancel").css('display','inline');
                      });

                      $("#cancel").on("click",function(){
                        $('.formValues').attr('action', "/books");
                        $(".formValues").css("display","none");
                        $("#id").val("");
                        $("#name").val("");
                        $("#author_id").val("");
                        $("#year").val("");
                        $("#price").val("");
                        $("#editorial_id").val("");
                        $("#pages").val("");

                        $('#check input').attr('value','post')
                        $("#create").val("Create");
                        $("#cancel").css('display','none');
                      });

                      $(".showNew").on("click",function(){
                        $(".formValues").css("display","inline");
                        $("#cancel").css('display','inline');
                      });

                      var cuantityBooks = 0;
                      var totalPrice = 0;

                      $(".add").on("click",function(e){
                        var id =  $(e)[0].currentTarget.dataset.id;
                        var price =  $(e)[0].currentTarget.dataset.price;

                        var totalBooks = parseInt($("#quantity"+id).val());
                            totalBooks += 1;
                            cuantityBooks++;
                            totalPrice += parseInt(price);
                            $("#quantity"+id).val(totalBooks);
                            $("#quantity_sent"+id).val(totalBooks);

                            $("#totalBooks").html(cuantityBooks);
                            $("#totalPrice").html(totalPrice);
                      });
                      $(".remove").on("click",function(e){
                        var id =  $(e)[0].currentTarget.dataset.id;
                        var price =  $(e)[0].currentTarget.dataset.price;

                        var totalBooks = parseInt($("#quantity"+id).val());
                            if(totalBooks>0) {
                              totalBooks -= 1;
                              cuantityBooks--;
                              totalPrice -= parseInt(price);
                            }
                            $("#quantity_sent"+id).val(totalBooks);
                            $("#quantity"+id).val(totalBooks);
                            $("#totalBooks").html(cuantityBooks);
                            $("#totalPrice").html(totalPrice);
                      });

                    });
                    </script>
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title-2 text-center">
                          <form class="formValues" action="/sales" method="post" name="salesDetails[]">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <table class="table-striped col-md-12 text-center">
                                <thead>
                                  <tr>
                                    <th>Total</th>
                                    <th>Name</th>
                                    <th>Author</th>
                                    <th>Year</th>
                                    <th>Price</th>
                                    <th>Editorial</th>
                                    <th>Pages</th>
                                    <th>Add +1</th>
                                    <th>Remove -1</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ( $books as $book)
                                    <tr>
                                        <td>
                                          <input type="number" value="0" id="quantity{{$book->id}}" class="form-control" disabled>
                                          <input type="hidden" name="quantity[]" value="0" id="quantity_sent{{$book->id}}" class="quantity">
                                          <input type="hidden" name="books_id[]" value="{{$book->id}}" id="bookid{{$book->id}}" class="books">
                                          <input type="hidden" name="unit_price[]" value="{{$book->price}}" id="unitprice{{$book->id}}" class="unit">
                                        </td>
                                        <td>{{$book->name}}</td>
                                        <td>
                                          @foreach( $authors as $author)
                                            @if ( $book->authors_id == $author->id )
                                              {{ $author->first_name}} {{ $author->last_name }}
                                            @endif
                                          @endforeach
                                        </td>
                                        <td>
                                          {{$book->year}}
                                        </td>
                                        <td>
                                          ${{$book->price}}
                                        </td>
                                        <td>
                                          @foreach( $editorials as $editorial)
                                            @if ( $book->editorials_id == $editorial->id )
                                              {{ $editorial->name }}
                                            @endif
                                          @endforeach
                                        </td>
                                        <td>
                                          {{$book->pages}}
                                        </td>
                                        <td>
                                            <div class="btn add"
                                                data-id='{{$book->id}}'
                                                data-name="{{$book->name}}"
                                                data-author="{{$book->authors_id}}"
                                                data-year="{{$book->year}}"
                                                data-price="{{$book->price}}"
                                                data-editorial="{{$book->editorials_id}}"
                                                data-pages="{{$book->pages}}"
                                                >+1</div></td>
                                        <td>
                                            <div class="btn btn-danger remove"
                                                data-id='{{$book->id}}'
                                                data-name="{{$book->name}}"
                                                data-author="{{$book->authors_id}}"
                                                data-year="{{$book->year}}"
                                                data-price="{{$book->price}}"
                                                data-editorial="{{$book->editorials_id}}"
                                                data-pages="{{$book->pages}}"
                                                type="submit">-1</div>
                                        </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                                <tfoot>
                                  <tr>
                                      <td><strong>Books:</strong> <span id="totalBooks">0</span></td>
                                      <td id="send"><strong>Total Price:</strong> $<span id="totalPrice">0</span></td>
                                      <td></td><td></td><td></td><td></td><td></td>
                                      <td class="text-right">Buy the books: <input type="submit" class="btn" value="Buy"></td>
                                  </tr>
                                </tfoot>
                            </table>
                          </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"> </div>
            </div>
        </div>
    </div>
  </div>

</body>
