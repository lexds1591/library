<style>
  div.header-title {

  }
  div.options {
    cursor: pointer;
    text-align: center;
    padding: 20px 15px;
    border-bottom: 1px solid #d4d9dd;
    box-shadow: inset 0px -2px 3px #e6e6e6, inset 0px -8px 3px #fdfdfd;
  }
  div.options > i {
        font-size: 25px;
        color: rgba(84, 84, 84, 0.75);
        padding: 12px 20px;
  }
  .col-md-12.options > a {
    position: absolute;
        margin: 11px 0px 0px -40px;
        text-transform: uppercase;
        text-decoration: none;
        background-color: #ffffff;
        padding: 7px 18px 7px 18px;
        border-radius: 5px 5px 5px 5px;
        color: #6c757d;
        opacity: 0;
        z-index: 0;
        transition: 0.5s all;
        border: 1px solid #f5f5f58c;
        box-shadow: -2px 1px 2px #d8d8d8;
  }
  div.options:hover > a {
    opacity: 1;
    margin: 5px 0px 0px -75px;
  }

div.menu-container {
    background-color: white;
}
div.header {
    background-color: #dc3545;
    color: white;
    text-align: center;
    padding: 30px 0px;
    font-size: 22px;
    cursor: pointer;
}
.head-title {
    background-color: white;
    padding: 10px 0px;
    border: 1px solid #ececec;
    box-shadow: 0px 4px 3px #dedede, inset 0px 0px 10px #fbfbfb;
    color: rgba(16, 89, 166, 0.5);
}
</style>
<script>
  $(document).ready(function(){
      $('.header').on('click',function(){
        location.href = '/';
      });
      $('.options').on('click',function(e){
        location.href = $(e)[0].currentTarget.dataset.url;
      });
  });
</script>
<div class="row">
  <div class="col-md-12 header">
      <span>BookStore</span>
  </div>
  <div class="col-md-12 options" data-url="/authors">
    <i class="ion-person-stalker" data-pack="default" data-tags="people, human, users, staff"></i>
    <a href="#">Authors</a>
  </div>
  <div class="col-md-12 options" data-url="/editorials">
    <i class="ion-document-text" data-pack="default" data-tags="paper, file"></i>
    <a href="#">Editorials</a>
  </div>
  <div class="col-md-12 options"  data-url="/books">
    <i class="ion-ios-bookmarks" data-pack="ios" data-tags="favorite"></i>
    <a href="#">Books</a>
  </div>
  <div class="col-md-12 options" data-url="/sales">
    <i class="ion-cube" data-pack="default" data-tags="box, square, container"></i>
    <a href="#">Sales</a>
  </div>
  <div class="col-md-12 options" data-url="/reports">
    <i class="ion-ios-paper" data-pack="ios" data-tags="feed, paper"></i>
    <a href="#">Reports</a>
  </div>
</div>
