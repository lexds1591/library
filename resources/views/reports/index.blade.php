

<!-- Header -->
@include('header.index',['title' => $title])
<body>


  <div class="container-fluid">
    <div class="row">
        <div class="col-md-1 menu-container" style="height: 100%;">
            @include('menu.index')
        </div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-3"> </div>
                <div class="col-md-6">
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title text-center"><h4>Wellcome to {{$title}}.</h4></div>
                    </div>

                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title-2 text-center">
                            <table class="table-striped col-md-12 text-center">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ( $reports as $report)
                                    <tr>
                                        <td>{{$report->name}}</td>
                                        <td>{{$report->quantity}}</td>
                                        <td>{{$report->total}}</td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"> </div>
            </div>
        </div>
    </div>
  </div>

</body>
