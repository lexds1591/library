<head>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{asset('/css/app.css',true) }}">
  <link rel="stylesheet" href="{{asset('/css/ionicons-2.0.1/css/ionicons.min.css',true) }}">

  <title>
    @if($title)
      {{ $title }}
    @else
      Problems with the page title
    @endif
  </title>
  <script type="text/javascript" src="{{asset('/js/app.js',true) }}"></script>
</head>
