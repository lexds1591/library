<!-- Header -->
@include('header.index',['title' => $title])
<body>


  <div class="container-fluid">
    <div class="row">
        <div class="col-md-1 menu-container" style="height: 100%;">
            @include('menu.index')
        </div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-3"> </div>
                <div class="col-md-6">
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title text-center"><h4>Wellcome to {{$title}}.</h4></div>
                    </div>
                    <div class="row new">
                        <div class="col-md-12 cont-add">
                          <form class="formValues" action="/authors" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <span id="check">
                              <input type="hidden" name="_method" value="post">
                            </span>
                            <input type="hidden" name="id" id="id">
                            <div class="form-group">
                              <label for="name">First name:</label>
                              <input type="text" class="form-control" name='first_name' id="first_name" minlength="5" required>
                            </div>
                            <div class="form-group">
                              <label for="name">Last name:</label>
                              <input type="text" class="form-control" name='last_name' id="last_name" minlength="5" required>
                            </div>
                            <div class="form-group text-right">
                              <input type="submit" value="Create" class="btn btn-success" id="create">
                              <input value="Cancel" style="display:none;" class="btn" id="cancel">
                            </div>
                          </form>
                        </div>
                    </div>
                    <script>
                    $(document).ready(function(){
                      $(".update").on("click",function(e){
                        var id =  $(e)[0].currentTarget.dataset.id;
                        var first_name =  $(e)[0].currentTarget.dataset.first_name;
                        var last_name =  $(e)[0].currentTarget.dataset.last_name;

                        $('.formValues').attr('action', "/authors/"+id);
                        $('#check input').attr('value','put')

                        $("#id").val(id);
                        $("#first_name").val(first_name);
                        $("#last_name").val(last_name);


                        $("#create").val("Update");
                        $("#cancel").css('display','inline-block');
                      });

                      $("#cancel").on("click",function(){
                        $("#id").val("");
                        $("#first_name").val("");
                        $("#last_name").val("");
                        $('#check input').attr('value','post')
                        $("#create").val("Create");
                        $("#cancel").css('display','none');
                        $('.formValues').attr('action', "/authors");

                      });

                    });
                    </script>
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title-2 text-center">
                            <table class="table-striped col-md-12 text-center">
                                <thead>
                                  <tr>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ( $authors as $author)
                                    <tr>
                                        <td>{{$author->first_name}}</td>
                                        <td>{{$author->last_name}}</td>
                                        <td><button class="btn update" data-id='{{$author->id}}' data-first_name="{{$author->first_name}}" data-last_name="{{$author->last_name}}">Update</button></td>
                                        <td>
                                          <form action="/authors/{{ $author->id }}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{ method_field('delete') }}
                                            <button class="btn btn-default" type="submit">Delete</button>
                                          </form>
                                        </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"> </div>
            </div>
        </div>
    </div>
  </div>

</body>
