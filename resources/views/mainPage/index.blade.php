<!-- Header -->
@include('header.index',['title' => 'Main page'])
<body>
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-1 menu-container" style="height: 100%;">
            @include('menu.index')
        </div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-3"> </div>
                <div class="col-md-6">
                  <div class="row" style="margin-top:20px;">
                      <!-- Body -->
                      <div class="col-md-12 text-center">
                        <img src="/images/books.jpg">
                      </div>
                  </div>
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title text-center"><h4>You're welcome.</h4></div>
                    </div>

                </div>
                <div class="col-md-3"> </div>
            </div>
        </div>
    </div>
  </div>

</body>
