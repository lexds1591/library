<!-- Header -->
@include('header.index',['title' => $title])
<body>


  <div class="container-fluid">
    <div class="row">
        <div class="col-md-1 menu-container" style="height: 100%;">
            @include('menu.index')
        </div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-3"> </div>
                <div class="col-md-6">
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title text-center"><h4>Wellcome to {{$title}}.</h4></div>
                    </div>
                    <div class="row new">
                        <div class="col-md-12 cont-add">
                          <form class="formValues" action="/editorials" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <span id="check">
                              <input type="hidden" name="_method" value="post">
                            </span>
                            <input type="hidden" name="id" id="id">
                            <div class="form-group">
                              <label for="name">New editorial name:</label>
                              <input type="text" class="form-control" name='name' id="name" minlength="5" required>
                            </div>
                            <div class="form-group text-right">
                              <input type="submit" value="Create" class="btn btn-success" id="create">
                              <input value="Cancel" style="display:none;" class="btn" id="cancel">
                            </div>
                          </form>
                        </div>
                    </div>
                    <script>
                    $(document).ready(function(){
                      $(".update").on("click",function(e){
                        var id =  $(e)[0].currentTarget.dataset.id;
                        var name =  $(e)[0].currentTarget.dataset.name;

                        $('.formValues').attr('action', "/editorials/"+id);
                        $('#check input').attr('value','put')
                        $("#id").val(id);
                        $("#name").val(name);
                        $("#create").val("Update");
                        $("#cancel").css('display','inline-block');
                      });

                      $("#cancel").on("click",function(){
                        $('.formValues').attr('action', "/editorials");
                        $("#id").val("");
                        $("#name").val("");
                        $('#check input').attr('value','post')
                        $("#create").val("Create");
                        $("#cancel").css('display','none');
                      });

                    });
                    </script>
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title-2 text-center">
                            <table class="table-striped col-md-12 text-center">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ( $editorials as $editorial)
                                    <tr>
                                        <td>{{$editorial->name}}</td>
                                        <td><button class="btn update" data-id='{{$editorial->id}}' data-name="{{$editorial->name}}">Update</button></td>
                                        <td>
                                          <form action="/editorials/{{ $editorial->id }}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{ method_field('delete') }}
                                            <button class="btn btn-default" type="submit">Delete</button>
                                          </form>
                                        </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"> </div>
            </div>
        </div>
    </div>
  </div>

</body>
