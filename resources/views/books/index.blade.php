<!-- Header -->
@include('header.index',['title' => $title])
<body>


  <div class="container-fluid">
    <div class="row">
        <div class="col-md-1 menu-container" style="height: 100%;">
            @include('menu.index')
        </div>
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title text-center"><h4>Wellcome to {{$title}}.</h4></div>
                    </div>
                    <div class="row new">
                        <div class="col-md-12 cont-add">
                          <form class="formValues" action="/books" method="post" style="display:none;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <span id="check">
                              <input type="hidden" name="_method" value="post">
                            </span>
                            <div class="form-group">
                              <label for="name">Name:</label>
                              <input type="text" class="form-control" name='name' id="name" minlength="5" required>
                            </div>
                            <div class="form-group">
                              <label for="author_id">Author:</label>
                              <select class="form-control" name="authors_id" id="author_id" required>
                                @foreach( $authors as $author)
                                  <option value="{{$author->id}}">{{$author->first_name}} {{$author->last_name}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="year">Year:</label>
                              <input type="number" class="form-control" name='year' id="year" required>
                            </div>
                            <div class="form-group">
                              <label for="price">Price:</label>
                              <input type="number" class="form-control" name='price' id="price" required>
                            </div>
                            <div class="form-group">
                              <label for="editorial_id">Editorial:</label>
                              <select class="form-control" name="editorials_id" id="editorial_id" required>
                                @foreach( $editorials as $editorial)
                                  <option value="{{$editorial->id}}">{{$editorial->name}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="name">Pages:</label>
                              <input type="number" class="form-control" name='pages' id="pages" minlength="5" required>
                            </div>
                            <div class="form-group text-right">
                              <input type="submit" value="Create" class="btn btn-success" id="create">
                              <input type="button" value="Cancel"  class="btn" id="cancel" style="display:none;">
                            </div>
                          </form>
                          <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                              <button type="button" class="btn showNew" name="button" >Add new</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <script>
                    $(document).ready(function(){
                      $(".update").on("click",function(e){
                        $(".formValues").css("display","inline");
                        var id =  $(e)[0].currentTarget.dataset.id;
                        var name =  $(e)[0].currentTarget.dataset.name;
                        var author =  $(e)[0].currentTarget.dataset.author;
                        var year =  $(e)[0].currentTarget.dataset.year;
                        var price =  $(e)[0].currentTarget.dataset.price;
                        var editorial =  $(e)[0].currentTarget.dataset.editorial;
                        var pages =  $(e)[0].currentTarget.dataset.pages;

                        $('.formValues').attr('action', "/books/"+id);
                        $('#check input').attr('value','put')
                        $("#id").val(id);
                        $("#name").val(name);
                        $("#author_id").val(author);
                        $("#year").val(year);
                        $("#price").val(price);
                        $("#editorial_id").val(editorial);
                        $("#pages").val(pages);
                        $("#create").val("Update");
                        $("#cancel").css('display','inline');
                      });

                      $("#cancel").on("click",function(){
                        $('.formValues').attr('action', "/books");
                        $(".formValues").css("display","none");
                        $("#id").val("");
                        $("#name").val("");
                        $("#author_id").val("");
                        $("#year").val("");
                        $("#price").val("");
                        $("#editorial_id").val("");
                        $("#pages").val("");

                        $('#check input').attr('value','post')
                        $("#create").val("Create");
                        $("#cancel").css('display','none');
                      });

                      $(".showNew").on("click",function(){
                        $(".formValues").css("display","inline");
                        $("#cancel").css('display','inline');
                      });
                    });
                    </script>
                    <div class="row">
                        <!-- Title -->
                        <div class="col-md-12 head-title-2 text-center">
                            <table class="table-striped col-md-12 text-center">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Author</th>
                                    <th>Year</th>
                                    <th>Price</th>
                                    <th>Editorial</th>
                                    <th>Pages</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ( $books as $book)
                                    <tr>
                                        <td>{{$book->name}}</td>
                                        <td>
                                          @foreach( $authors as $author)
                                            @if ( $book->authors_id == $author->id )
                                              {{ $author->first_name}} {{ $author->last_name }}
                                            @endif
                                          @endforeach
                                        </td>
                                        <td>
                                          {{$book->year}}
                                        </td>
                                        <td>
                                          ${{$book->price}}
                                        </td>
                                        <td>
                                          @foreach( $editorials as $editorial)
                                            @if ( $book->editorials_id == $editorial->id )
                                              {{ $editorial->name }}
                                            @endif
                                          @endforeach
                                        </td>
                                        <td>
                                          {{$book->pages}}
                                        </td>
                                        <td>
                                            <button class="btn update"
                                                data-id='{{$book->id}}'
                                                data-name="{{$book->name}}"
                                                data-author="{{$book->authors_id}}"
                                                data-year="{{$book->year}}"
                                                data-price="{{$book->price}}"
                                                data-editorial="{{$book->editorials_id}}"
                                                data-pages="{{$book->pages}}"
                                                >Update</button></td>
                                        <td>
                                          <form action="/books/{{$book->id}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            {{ method_field('delete') }}
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                          </form>
                                        </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"> </div>
            </div>
        </div>
    </div>
  </div>

</body>
