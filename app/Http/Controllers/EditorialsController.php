<?php

namespace App\Http\Controllers;

use App\Models\Editorial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class EditorialsController extends Controller
{
    /**
     * Main page for editorials showing all the records
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $editorials = (new Editorial)->newQuery();

        if ($request->has('name')){
            $editorials->where('name','like','%'.$request->input('name').'%');
        }
        return view("editorials/index",['title' => 'Editorials', 'editorials' => $editorials->get()]);
    }

    /**
     * Find an editorial using the id
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function find($id)
    {
        $data = Editorial::find($id);
        return view("editorials/index",['title' => 'Editorials', 'editorials' => $data->get()]);
    }

    /**
     * Add a new record and retrieves it.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $data = Editorial::create($request->all());
        return $this->find($data->id);
    }

    /**
     * Update a record based on the id and the params given.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $editorial = Editorial::find($id);
        if (!is_null($editorial))
            $editorial->update($request->all());
        return $this->find($id);
    }

    public function delete($id)
    {
        Editorial::destroy($id);
        return redirect('/editorials/');

    }
}
