<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\SaleDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    /**
     * Main page for reports, shows the reports according with the parameters sent.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index (Request $request)
    {
        $startDate = $request->input('start_date') ?? Carbon::minValue();
        $endDate = $request->input('end_date') ?? Carbon::now();
        $entity = $request->input('entity') ?? 'books';

        $reports = new Sale();
        $reports = $reports->salesReportQuery($entity, $startDate, $endDate);
        return view("reports/index",['title' => 'Reports', 'reports' => $reports->get()]);

    }
}
