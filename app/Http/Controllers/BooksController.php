<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Models\Editorial;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
     * Main page for books showing all the records
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $books = Book::with('authors', 'editorials');

        if ($request->has('name')) {
            $books->where('name', 'like', '%' . $request->input('name') . '%');
        }

        return view("books/index",
            [
                'title' => 'Books',
                'books' => $books->get(),
                'editorials' => Editorial::all(),
                'authors' => Author::all()
            ]
        );
    }

    /**
     * Find a book using the id
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function find($id)
    {
        $data = Book::find($id);
        return view("books/index",['title' => 'Books', 'books' => $data->get()]);
    }

    /**
     * Add a new record and retrieves it.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $data = Book::create($request->all());
        $books = Book::with('authors', 'editorials');
        return view("books/index",
            [
                'title' => 'Books',
                'books' => $books->get(),
                'editorials' => Editorial::all(),
                'authors' => Author::all()
            ]
        );
    }

    /**
     * Update a record based on the id and the params given.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        if (!is_null($book))
            $book->update($request->all());
            $books = Book::with('authors', 'editorials');
            return view("books/index",
                [
                    'title' => 'Books',
                    'books' => $books->get(),
                    'editorials' => Editorial::all(),
                    'authors' => Author::all()
                ]
            );
    }

    /**
     * Delete a record by id
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Book::destroy($id);
        $books = Book::with('authors', 'editorials');
        return view("books/index",
            [
                'title' => 'Books',
                'books' => $books->get(),
                'editorials' => Editorial::all(),
                'authors' => Author::all()
            ]
        );
    }
}
