<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{

    /**
     * Main page for authors showing all the records
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $authors = (new Author)->newQuery();

        if($request->has('first_name')) {
            $authors->where('first_name','like','%'.$request->input('first_name').'%');
        }
        if($request->has('last_name')) {
            $authors->where('last_name','like','%'.$request->input('last_name').'%');
        }

        return view("authors/index",['title'=>'Authors','authors'=>$authors->get()]);
    }

    /**
     * Find an author by id
     *
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function find($id)
    {
        $data = Author::find($id);
        return response(['title' => 'Authors', 'authors' => $data]);
    }

    /**
     * Add a new author and retrieves it.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $data = Author::create($request->all());
        return view("authors/index",['title'=>'Authors','authors'=>$data->get()]);

    }

    /**
     * Update a record based on the id and the params given.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $author = Author::find($id);
        if (!is_null($author))
            $author->update($request->all());

        return view("authors/index",['title'=>'Authors','authors'=>$author->get()]);
    }

    /**
     * Deletes an author based on the id sent
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($id)
    {
        Author::destroy($id);
        return redirect('/authors/');
    }
}
