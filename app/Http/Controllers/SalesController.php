<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\Book;
use App\Models\Editorial;
use App\Models\Author;
use App\Models\SaleDetail;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Returns the Sales view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $books = Book::with('authors', 'editorials');
        return view("sales/index",['title' => 'Sales',
        'books' => $books->get(),
        'editorials' => Editorial::all(),
        'authors' => Author::all()]);
    }

    /**
     * Saves the Sale and its respective SaleDetails
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
      $arra = [];
      $quantity = [];
      $books = [];
      $unit = [];
      $c = 0;
      $res = $request->all();
      unset($res['_token']);

      foreach ($res as $key => $value) {
          $arra[$c] = $value;
          $c++;
      }

      $c = 0;
      for($i = 0; $i < count($arra[0]); $i++) {
        if($arra[0][$i] > 0) {
          $unit[$i] = [ "unit_price" =>$arra[2][$i], "books_id" => $arra[1][$i], "quantity" =>$arra[0][$i]];
        }
      }

      $salesDetails = $unit;
      $data = Sale::create();

      foreach ($salesDetails as $salesDetail) {
        $data->salesDetails()->save(new SaleDetail($salesDetail));
      }

      $books = Book::with('authors', 'editorials');
      return view("sales/index",['title' => 'Sales',
      'books' => $books->get(),
      'editorials' => Editorial::all(),
      'authors' => Author::all(),
      'message' => 'Cool you bought a lot of books !!']);
    }
}
