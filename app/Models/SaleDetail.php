<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{

    /**
     * Indicates that this model doesn't use timestamps
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sales_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'books_id',
        'sales_id',
        'quantity',
        'unit_price'
    ];

    /**
     * Relation one to many (inverse) with books
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function books()
    {
        return $this->belongsTo('App\Models\Book', 'books_id');
    }

    /**
     * Relation one to many (inverse) with sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sales()
    {
        return $this->belongsTo('App\Models\Sale', 'sales_id');
    }

}
