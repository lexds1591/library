<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sale extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sales';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable =[];
    
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relation one to many with sales_details
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salesDetails()
    {
        return $this->hasMany('App\Models\SaleDetail','sales_id');
    }

    /**
     *Return the reports, it could be books, authors or editorials reports, according with the dates specified.
     *
     * @param $entity
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    public function salesReportQuery($entity, $startDate, $endDate)
    {
        $salesDetailsTable = with(new SaleDetail)->getTable();
        $booksTable = with(new Book())->getTable();
        $name = ($entity == 'authors') ? "concat(first_name, ' ',last_name) as name" : $entity.'.name';

        $reports = DB::table($this->table)
            ->join($salesDetailsTable, $this->table.'.id', '=', 'sales_id')
            ->join($booksTable, $salesDetailsTable.'.books_id', '=', $booksTable.'.id');
        if($entity != 'books') {
            $reports->join($entity, $booksTable.'.' . $entity . '_id', '=', $entity . '.id');
        }
         $reports->select(DB::raw($name.', sum(quantity) as quantity, sum(quantity*unit_price) as total'))
            ->whereBetween($this->table.'.created_at', [$startDate, $endDate])
            ->groupBy($entity.'.id')
            ->orderBy('total', 'DESC');

        return $reports;
    }
}
