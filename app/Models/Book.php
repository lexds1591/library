<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'authors_id',
        'year',
        'price',
        'editorials_id',
        'pages'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Relation one to many (inverse) with editorials
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editorials()
    {
        return $this->belongsTo('App\Models\Editorial', 'editorials_id');
    }

    /**
     * Relation one to many (inverse) with authors
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authors()
    {
        return $this->belongsTo('App\Models\Author', 'authors_id');
    }
}
